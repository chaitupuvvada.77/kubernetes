# OICT Kubernetes

## Dashboard UI

Nastavím si k sobě proxy:

`kubectl proxy`

Zjistím bearer token:

`kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"`

a použiju pro přístup na GUI:

[kubernetes-dashboard](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)

## User sekce - základní `kubectl` příkazy

Ověření správně vytvořené konfigurace `.kube/config`

```
kubectl config view
```

Výpis dostupných kontextů:

```
kubectl config get-contexts
```

Výpis dostupných namespaces:

```
kubectl get ns
```

### context změna clusteru nebo namespace

Přepnutí na dev cluster `k8s01dev`

`kubectl config use-context k8s01dev`

Změna kontextu na namespace `golemio`

`kubectl config set-context --current --namespace=golemio`

Příklad pro namespace `parking-dev`

`kubectl config set-context --current --namespace=parking-dev`

změna prostředí na stage pro Parkování

`kubectl config set-context --current --namespace=parking-stage`

### pod

Výpis dostupných podů v rámci current namespace

`kubectl get pods`

alternativa se specifikací namespace `queue`, kde běží rabbit

`kubectl get pods -n queue`

Restart podu - mohu provést snížením scale na hodnotu 0 nebo smazáním podu
`kubectl delete pod <podname>`

### describe pod

Popis deploymentu podu včetně ENV a resources. Vhodné také na troubleshoting startu kontejneru, ve výpisu na konci v sekci `Events`.

```
kubectl describe pod  <podname>
```

### service

```
kubectl get service
```

### port-forward

Přesměrování rabbitmq k sobě na localhost

#### RabbitMQ

```
kubectl port-forward -n queue service/rabbitmq 15672
```

[http://localhost:15672/](http://localhost:15672/)

#### Kibana logs

```
kubectl port-forward -n logging service/quickstart-kb-http 5601
```

[http://localhost:5601/](http://localhost:5601/)

#### redis golemio

```
kubectl port-forward -n database service/redis-master 6379
```

### log

Vypsání logu přímo z běžícího podu na STDOUT, ideální pro případnou bližší lokalizaci problému.

```
kubectl logs <POD-NAME>
```

Poznámka: jmeno podu lze doplnovat klávesou <TAB>

Příklad vypsání posledních 100 řádek logu za poslední hodinu:

```
kubectl logs název_podu --tail 100 --since 1h
```

### kubectl exec shell

Run a shell command in pod:

```
kubectl exec -it <POD_NAME> -- bash
```

V shellu pak mohu projít obsah kontejneru, ENV proměnné atd.

## Admin sekce - pro zkušené kubernetes borce

### create namespace

Spolu s vytvořením namespace nastavit i label `name=`

```bash
namespace=visual

kubectl create namespace $namespace
kubectl label namespace $namespace name=$namespace
```

## Gitlab

Integrace Kubernetes s Gitlab nastavena dle [add-existing-cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster).

V definici gitlab CI pipeline je nutno pouzit `environment` dle vzoru:

```
  environment:
    name: development
    url: https://example.com
```

### GitLab Deploy Token

If a user creates one named `gitlab-deploy-token`, the username and token of the Deploy Token is automatically exposed to the CI/CD jobs as environment variables: `CI_DEPLOY_USER` and `CI_DEPLOY_PASSWORD`

## Kubernetes security

Vytváření docker images `Dockerfile` v souladu s nařízením odboru bezpečnosti:

- securityContext: runAsNonRoot: true - docker image neběží pod root-em
- EXPOSE port 8080 namísto 80 (nevyžaduje root)
- multi-stage build - oddělená zdrojová image pro build a pro minimalistická varianta běh docker runtime

## RBAC users

### kubernetes uživatel

vytvoření certificate signing request (CSR) pro přístup na kubernetes cluster:

`openssl req -new -newkey rsa:4096 -nodes -keyout <username>.key -out <username>.csr -subj "/CN=<username>/O=developer"`

privátní klíč schovat a certifikáční request CSR poslat na hana nebo srba.

### kubernetes administrátor

```bash
export user=<username>
export cluster="k8s02prod"

openssl req -in $user.csr -noout -text

export base64_csr=`(cat $user.csr | base64 | tr -d '\n')`

cat create-user-request.yaml | envsubst  | kubectl apply -f-

kubectl get csr

kubectl certificate approve $cluster-$user

kubectl get csr/$cluster-$user -o yaml

kubectl get csr $cluster-$user -o jsonpath='{.status.certificate}' \
  | base64 --decode > $user.crt

openssl x509 -in $user.crt -noout -text
```

### kubernetes uživatel

Email/slack obdržíte user certifikat. Na posledni radky v `$HOME/.kube/config` je potreba doplnit svuj base64 private key a získaný certfikat

`cat <user>.key | base64 | tr -d '\n'`

Kube config je nutné složit dle vzoru (https://gitlab.com/operator-ict/devops/kubernetes/-/blob/master/create-k8s-user-developer/kube-config-template) a uložit jako `$HOME/.kube/config`.

V template config je uveden context jak dev, tak prod kubernetes cluster geetoo cluster.
